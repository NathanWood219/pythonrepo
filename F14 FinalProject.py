# Final Project - Nathan Wood (ID#109876625)
# Bifid Cipher encoding/decoding

import random;

# Declaring the encode() function
def encodeBifid(keyString,keyPeriod,encodeString):
    # Initialize variables that will store parts of the encoded string
    encodeRow = ''; encodeCol = ''; encodeOutput ='';
    
    # Pairs of numbers in keyNumber correspond to the characters in keyString (in order); this string is a constant
    keyNumber = '111213141516212223242526313233343536414243444546515253545556616263646566';
    # Another constant, this is just every unique character in the keystring for comparison
    baseString = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    
    # STEP 1a: Find the row number for each character
    for c in encodeString:
        if(baseString.find(c)!=-1):
            location = keyString.find(c)*2;
            encodeRow += keyNumber[location];
            
    # STEP 1b: Find the column number for each character
    for c in encodeString:
        if(baseString.find(c)!=-1):
            location = keyString.find(c)*2;
            encodeCol += keyNumber[location+1];
        
    # STEP 2: Combine the corresponding groups based off the period
    i = 0;
    encodeString = '';
    length = len(encodeRow)/keyPeriod;
    
    while(i<=length):
        # Current position in encode_row and encode_col being added
        position = i*keyPeriod;
        
        if(len(encodeRow)%keyPeriod>0) and (i==length):
            # Find extra amount to add
            extraChars = len(encodeRow)%keyPeriod;
            
            # Add ONLY the extra amount from the column and row strings
            encodeString += encodeRow[position:position+extraChars] + encodeCol[position:position+extraChars];
        else:
            # Add the column and row to encode_string based off key_period
            encodeString += encodeRow[position:position+keyPeriod] + encodeCol[position:position+keyPeriod];
        
        i += 1;
    
    # STEP 3: Translate back into characters
    i = 0;
    # Scan through the encoded numbers in pairs
    while(i<len(encodeString)):
        j = 0;
        # Scan through the key grid in pairs
        while(j<len(keyNumber)):
            # If the pair in the encoded string and the pair in the key match...
            if(encodeString[i:i+2]==keyNumber[j:j+2]):
                # ...add the corresponding character from keystring and end inner loop
                encodeOutput += keyString[j/2];
                j = len(keyNumber);
            j += 2;
        i += 2;
    
    # STEP 4: Return the encoded message
    return encodeOutput;
    
# Decodes the input string using the keystring, number, and period
def decodeBifid(keyString,keyPeriod,decodeString):
    # Initialize variables that will store parts of the encoded string
    decodeRow = ''; decodeCol = ''; new_decodeString = '';
    decodeOutput = '';
    
    # Pairs of numbers in keyNumber correspond to the characters in keyString (in order); this string is a constant
    keyNumber = '111213141516212223242526313233343536414243444546515253545556616263646566';
    # Another constant, this is just every unique character in the keystring for comparison
    baseString = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    
    # Step 1: Translate letters into numbers
    for c in decodeString:
        if(baseString.find(c)!=-1):
            location = keyString.find(c);
            new_decodeString += keyNumber[location*2:(location*2)+2];
    
    # Step 2: Split into rows and columns based off the period
    i = 0;
    length = len(new_decodeString)/(keyPeriod*2);
        
    while(i<=length):
        # Current position in encode_row and encode_col being added
        position = i*keyPeriod*2;
        
        if(len(new_decodeString)%(keyPeriod*2)>0) and (i==length):
            # Find extra amount to add
            extraChars = (len(new_decodeString)%(keyPeriod*2))/2;
            
            # Add ONLY the extra amount from new_decodeString to the rows and columns
            decodeRow += new_decodeString[position:position+extraChars];
            decodeCol += new_decodeString[position+extraChars:position+(extraChars*2)];
        else:
            # Add the numbers to the rows and columns to new_decodeString based off the period
            decodeRow += new_decodeString[position:position+keyPeriod];
            decodeCol += new_decodeString[position+keyPeriod:position+(keyPeriod*2)];
        
        i += 1;
    
    # Step 3: Translate back into letters
    i = 0;
    while(i<len(decodeRow)):
        j = 0;
        while(j<len(keyNumber)):
            if(decodeRow[i] + decodeCol[i]==keyNumber[j:j+2]):
                decodeOutput += keyString[j/2];
                j = len(keyNumber);
            j += 2;
        i += 1;
        
    # Step 4: Return the decoded message
    return decodeOutput;

# This function checks the user input key string against a base,
# returning True if the key string is valid and False if it is not
def keyStringTest(keyString):
    baseString = 'ABCDEFGHIKLMNOPQRSTUVWXYZ0123456789';
    
    i = 0;
    while(i!=35):
        for c in baseString:
            # Make sure that the character exists in keyString and that it only occurs once
            if(keyString.find(c)!=-1) and (keyString.count(c)==1):
                i += 1;
            else:
                return False;     
    return True;

# This function tests the input of the user, making sure that it is an integer
# within the required range; checks until the user inputs a valid number.
# Returns the value of varCheck once it is correctly submitted
def userInputTest(varCheck,rangeLow,rangeHigh,requestText,errorText):
    try:
        varCheck = int(raw_input(requestText));
    except ValueError:
        varCheck = -1;
    
    # Making sure that varCheck is an integer between rangeLow and rangeHigh
    while(varCheck<rangeLow) or (varCheck>rangeHigh):
        print errorText;
        try:
            varCheck = int(raw_input(requestText));
        except ValueError:
            varCheck = -1;
    return varCheck;
    
# Default keysquare string and period
public_keyString = 'FO3DPER01XQAK5U9H6CVY4LBSN8WIZJ2MT7G';
public_keyPeriod = 5;

# Default 6x6 keysquare
#     1 2 3 4 5 6
#    ------------
# 1 | F O 3 D P E
# 2 | R 0 1 X Q A
# 3 | K 5 U 9 H 6
# 4 | C V Y 4 L B
# 5 | S N 8 W I Z
# 6 | J 2 M T 7 G

# Menu selection variables, initialize out of range
userSelect = -1;
userSubselect = -1;

# Printing startup text
print '\n|  Bifid Cipher Project  |';
print '| Written by Nathan Wood |';
print '|      November 2014     |';

# Continue unless the user quits
while(userSelect!=4):
    
    # If userSubselect != -1, then go back to the current submenu instead of the main menu
    if(userSubselect==-1):
        # Printing the menu
        print '\n--- Main Menu Options ---';
        print '0. Encode a message';
        print '1. Decode a message';
        print '2. View/edit the key';
        print '3. View/edit the period';
        print '4. Quit';
        
        # Retrieve user menu option and check that it is a valid input
        userSelect = userInputTest(userSelect,0,4,'Selection: ','Invalid selection');
    
    # Encoding a message
    if(userSelect==0):
        # Retrieve user input
        encodeString = raw_input('\nEnter a message to encode: ').upper();        
        
        # Print out the encoded string using the encodeBifid() function
        print 'Encoded message:',encodeBifid(public_keyString,public_keyPeriod,encodeString);
        
        # Set userSubselect = 4 so that the program pauses before printing the menu again
        userSubselect = 4;
        
    # Decoding a message
    elif(userSelect==1):
        # Retrieve user input
        decodeString = raw_input('\nEnter a message to decode: ').upper();
        
        # Print out the decoded string using the decodeBifid() function
        print 'Decoded message:',decodeBifid(public_keyString,public_keyPeriod,decodeString);
        
        # Set userSubselect = 4 so that the program pauses before printing the menu again
        userSubselect = 4;
    
    # Changing the key string
    elif(userSelect==2):
        # Controls the submenu; initialize out of range
        userSubselect = -1;
        
        # Print the submenu for the key string options
        print '\n----- Key String Options -----';
        print '0. View the current key string';
        print '1. Generate random key string';
        print '2. Enter a custom key string';
        print '3. Return';
        
        userSubselect = userInputTest(userSubselect,0,3,'Selection: ','Invalid selection');
        
        # Fixing spacing
        if(userSubselect!=3):
            print '';
        
        if(userSubselect==0):
            # Print the current key string
            print 'The current key string is',public_keyString;
            
        elif(userSubselect==1):
            # Randomly generating a new key string
            
            # This function randomly pulls a letter from base_string
            # with the chance based off how far into the string it is
            baseString = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
            public_keyString = '';
            while(len(public_keyString)!=36):
                i = 36;
                for c in baseString:
                    random.seed();
                    if(public_keyString.find(c)==-1) and (random.randint(0,i)==0):
                        public_keyString += c;
                    i -= 1;
            
            # Print the randomly generated string
            print 'The new key string is',public_keyString;
            
        elif(userSubselect==2):
            # Allowing the user to input a custom key string
            
            # Retrieve initial user input for the new key string
            test_keyString = str(raw_input('Enter the 36 character key string (A-Z, 0-9): ')).upper();
            isUnique = keyStringTest(test_keyString);
            
            # Making sure that the new key string is 36 characters long
            while(len(test_keyString)!=36) or (isUnique==False):
                # Identify the problem to the user
                if(len(test_keyString)!=36):
                    print 'That key string was not the correct length of 36 characters';
                else:
                    print 'All of the letters and digits must be unique';
                    
                # Request a new input and test it against the base string
                test_keyString = str(raw_input('Enter the 36 character key string (A-Z, 0-9): ')).upper();
                isUnique = keyStringTest(test_keyString);
                
            # Now that the key string has been confirmed, set the real key string to the test version
            public_keyString = test_keyString;
            
            print 'New key string has been accepted';
        else:
            userSubselect = -1;
    
    # Changing the period
    elif(userSelect==3):
        # Controls the submenu; initialize out of range
        userSubselect = -1;
        
        # Print the submenu for the period options
        print '\n-------- Period Options --------';
        print '0. View the current period';
        print '1. Generate random period (2-10)';
        print '2. Enter a custom period (2-10)';
        print '3. Return';
        
        userSubselect = userInputTest(userSubselect,0,3,'Selection: ','Invalid selection');
        
        # Fixing spacing
        if(userSubselect!=3):
            print '';
        
        if(userSubselect==0):
            # Print the current period
            print 'The current period is',public_keyPeriod;
        
        elif(userSubselect==1):
            # Randomly generate a new period between 2 and 10
            random.seed();
            public_keyPeriod = random.randint(2,10);
            print 'The new period is',public_keyPeriod;
            
        elif(userSubselect==2):
            # Allowing the user to input a custom period
            
            public_keyPeriod = userInputTest(public_keyPeriod,2,10,'Enter the new period (2-10): ','That number is out of the range');
            
            # Once the input has been accepted, let the user know
            print 'The new period has been accepted';
        else:
            userSubselect = -1;
            
    # Wait for the user to press enter to continue back to the menu
    if(userSelect!=4) and (userSubselect>=0):
        # Resetting userSubselect
        if(userSelect!=2) and (userSelect!=3):
            userSubselect = -1;
        raw_input('Press enter to continue');