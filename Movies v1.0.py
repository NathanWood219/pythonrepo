import os;

raw_input('Press ENTER to scan for movies and covers...');

# Identifies which files don't line up with a cover
# NOTE: Currently there is a bug that it does not scan the very last movie

# Also very poorly documented...

def printList(list):
    for i in range(0, len(list)):
        print '\t' + str(i+1) + '\t', list[i];
        
def parseFilename(filename):
    for i in range(len(filename)-1, 0, -1):
        if filename[i] == '.':
            return filename[0:i];
    return 'null';

# Hard coded directories for movies and covers
movies = os.listdir('E:\\1. Movies');
covers = os.listdir('E:\\1. Movies\\Covers');

missingCoversList = [];
extraCoversList = [];
missingYearsList = [];
mval = 0; cval = 0;
errors = 0;

while mval < len(movies) and cval < len(covers) :
    mov = movies[mval];
    if mov == 'Covers':
        mval += 1;
        mov = movies[mval];
    movTitle = parseFilename(mov);
    if movTitle[len(movTitle)-1:len(movTitle)] != ')' or movTitle[len(movTitle)-6:len(movTitle)-5] != '(':
        missingYearsList.append(movTitle);
        #print '\nTitle Error:',movTitle,'is missing the year\n';
        errors += 1;
    else:
        movTitle = movTitle[0:len(movTitle)-7];
    cov = covers[cval];
    if cov == 'Thumbs.db':
        cval += 1;
        cov = covers[cval];
    covTitle = parseFilename(cov);
    if movTitle.lower() == covTitle.lower():
        #print mov,'matches',cov;
        cval += 1;
        mval += 1;
    else:
        #print '\nMatch Error:\t',mov,'\n\t\t',cov,'\n';
        errors += 1;
        if movTitle.lower() < covTitle.lower():
            mval += 1;
            missingCoversList.append(movTitle);
        else:
            cval += 1;
            extraCoversList.append(covTitle);
            
print '\nMatches:',str(len(movies)-errors) + '/' + str(len(movies));
print 'Errors:',errors;
print 'Missing covers:',len(missingCoversList);
printList(missingCoversList);
print 'Extra covers:',len(extraCoversList);
printList(extraCoversList);
print 'Movies missing years:',len(missingYearsList);
printList(missingYearsList);

raw_input("\nPress enter to quit...");